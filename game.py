#Prompt User for name
username = input("What's your name? ")
print("Hello, " + username,"!")
guess_number = 1
#Announce Guess
for guess_number in range (6):
    #Generate guesses
    from random import randint
    month = randint(1, 12)
    year = randint(1924, 2004)
    #Form guess
    print (username, ", were you born in ", month, "/", year, "?")
    #Prompt User answer
    answer = input("Yes or No? ")
    if answer == ("Yes"):
        print("I knew it!")
        exit()
    elif guess_number >= 5:
        print("I give up.")
        break
    else:
        print("Okay let me try again!")
        guess_number += 1